import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Dexsys {

    public static void main(String[] args) throws IOException {

        Boolean exit = true;
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Введите команду: ");
        String comand = reader.readLine();

        myMethod myArray = new myMethod();

        while (exit) {

            switch (comand) {
                case "init array":
                    myArray.initArray();
                    System.out.println("Введите команду: ");
                    comand = reader.readLine();
                    break;
                case "print":
                    myArray.Print();
                    System.out.println("Введите команду: ");
                    comand = reader.readLine();
                    break;
                case "print X":
                    myArray.PrintType("X");
                    System.out.println("Введите команду: ");
                    comand = reader.readLine();
                    break;
                case "print S":
                    myArray.PrintType("S");
                    System.out.println("Введите команду: ");
                    comand = reader.readLine();
                    break;
                case "print M":
                    myArray.PrintType("M");
                    System.out.println("Введите команду: ");
                    comand = reader.readLine();
                    break;
                case "clear X":
                    myArray.ClearType("X");
                    System.out.println("Введите команду: ");
                    comand = reader.readLine();
                    break;
                case "clear S":
                    myArray.ClearType("S");
                    System.out.println("Введите команду: ");
                    comand = reader.readLine();
                    break;
                case "clear M":
                    myArray.ClearType("M");
                    System.out.println("Введите команду: ");
                    comand = reader.readLine();
                    break;
                case "merge":
                    myArray.Merge();
                    System.out.println("Введите команду: ");
                    comand = reader.readLine();
                    break;
                case "any more":
                    myArray.anyMore();
                    System.out.println("Введите команду: ");
                    comand = reader.readLine();
                    break;
                case "help":
                    myArray.help();
                    System.out.println("Введите команду: ");
                    comand = reader.readLine();
                    break;
                case "exit":
                    exit = false;
                default:
                    System.out.println("Введите правильную команду");
                    System.out.println("Введите команду: ");
                    comand = reader.readLine();
                    break;
            }
        }
    }
}
