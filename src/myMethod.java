import java.util.*;

public class myMethod {

    private  int[] mainArray;
    private  List X;
    private  List S;
    private  List M;

    List res = new ArrayList<>();

        public  void initArray(){

        int max = 100;

        mainArray = new int[100];
        X = new ArrayList();
        S = new ArrayList();
        M = new ArrayList();

        for (int i = 0; i < mainArray.length; i++) {

            mainArray[i] =  (int)(Math.random() * max);

        }

        Arrays.sort(mainArray);

        for (int i = 0; i < mainArray.length ; i++) {

            if (mainArray[i] % 3 == 0 && mainArray[i] != 0){

                X.add(mainArray[i]);

            }

            if (mainArray[i] % 7 == 0 && mainArray[i] != 0){

                S.add(mainArray[i]);

            }

            if (mainArray[i] % 21 == 0 && mainArray[i] != 0){

                M.add(mainArray[i]);

            }
        }
    }

    public  void Print(){

        System.out.println(Arrays.toString(mainArray));

        if (S.size() != 0) {
            System.out.println(X.toString());
        }
        else {

            System.out.println("Список X пуст");
        }

        if (S.size() != 0) {
            System.out.println(S.toString());
        }
        else {

            System.out.println("Список S пуст");
        }

        if (M.size() != 0) {
            System.out.println(M.toString());
        }
        else {

            System.out.println("Список M пуст");
        }

    }

    public  void PrintType(String B){

        if (B.equals("X"))

            if (X.size() != 0) {
                System.out.println(X.toString());
            }
            else {

                System.out.println("Список X пуст");
            }

        if (B.equals("S"))

            if (S.size() != 0) {
                System.out.println(S.toString());
            }
            else {

                System.out.println("Список S пуст");
            }

        if (B.equals("M"))

            if (M.size() != 0) {
                System.out.println(M.toString());
            }
            else {

                System.out.println("Список M пуст");
            }
    }

    public void ClearType(String B){

        if (B.equals("X"))

            X.clear();

        if (B.equals("S"))

            S.clear();

        if (B.equals("M"))

            M.clear();

    }

    public void Merge(){

        res.addAll(X);
        res.addAll(S);
        res.addAll(M);

        TreeSet<String> myset = new TreeSet<String>(res);

        System.out.println(myset.toString());

        ClearType("X");
        ClearType("S");
        ClearType("M");

    }

    public void anyMore(){

        Boolean flag = false;

        for (int i = 0; i < mainArray.length; i++) {

           if  (!X.contains(mainArray[i])){
               flag = true;
               break;
           }
            if  (!S.contains(mainArray[i])){
                flag = true;
                break;
            }
            if  (!M.contains(mainArray[i])){
                flag = true;
                break;
            }
        }

        System.out.println(flag);
    }

    public void help(){

        System.out.println("init array	- инициализация списков набором значений array");
        System.out.println("print - печать всех списков");
        System.out.println("print type - печать конкретного списка, где type принимает значения X,S,M");
        System.out.println("any more - выводит на экран были ли значения не вошедшие ни в один список");
        System.out.println("clear type - чистка списка , где type принимает значения X,S,M");
        System.out.println("merge - слить все списки в один вывести на экран и очистить все списки");
        System.out.println("help - вывод справки по командам");
        System.out.println("exit - выход");

    }

}
